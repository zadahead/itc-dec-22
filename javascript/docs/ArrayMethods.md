## Array Methods
Array methods are functions built-in to JavaScript that we can apply to our arrays — Each method has a unique function that performs a change or calculation to our array and saves us from writing common functions from scratch.





## .forEach()
The `forEach()` method executes a provided function for each array element.

```js
const numbers = [28, 77, 45, 99, 27];

numbers.forEach(number => {  
  console.log(number);
}); 
/* log
22
77
45
99
27
*/
```

## .map()
The `map()` method creates a new array with the results of calling a function for every array element.

```js
//map
const users = [
    { id: 1, name: 'Mosh' },
    { id: 2, name: 'David' }
]
const mappedList = users.map((user) => (
        {
            id: user.id,
            name: user.name
        }
    ))
console.log(mappedList);
/* log
[
    {
        "id": 1,
        "name": "Mosh"
    },
    {
        "id": 2,
        "name": "David"
    }
]
*/
```


## .filter()
The `filter()` method returns a new array with all elements that pass the test defined by the given function.

```js
//filter
const users = [
    { id: 1, name: 'Mosh' },
    { id: 2, name: 'David' }
]

const filteredList = users.filter((user) => {

    if (user.id === 1) {
        return true;
    }
    return false;

})
console.log(filteredList);
/* log
[
    {
        "id": 1,
        "name": "Mosh"
    }
]
*/
```

## .sort()
The `sort()` method sorts the items of an array in a specific order (ascending or descending).

```js
//sort
const users = [
    { id: 1, name: 'Mosh' },
    { id: 2, name: 'Alon' }
]

users.sort((a, b) => {
    if (a.name > b.name) {
        return 1;
    }
    return -1;
})
console.log(users);
/* log
[
    {
        "id": 2,
        "name": "Alon"
    },
    {
        "id": 1,
        "name": "Mosh"
    }
]
*/
```


## .reduce()
The `reduce()` method executes a reducer function on each element of the array and returns a single output value.

```js
const arrayOfNumbers = [1, 2, 3, 4];

const sum = arrayOfNumbers.reduce((accumulator, currentValue) => {  
  return accumulator + currentValue;
}, 0);

console.log(sum);
/* log
10
*/
```

for more methods, go here: 
https://www.programiz.com/javascript/library/array

