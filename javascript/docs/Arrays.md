# Array
The Array object, enables storing a collection of multiple items under a single variable name

```js
const cars = ["Saab", "Volvo", "BMW"];
```

## Accessing Array Elements
You access an array element by referring to the index number:

```js
const cars = ["Saab", "Volvo", "BMW"];
let car = cars[0];
```

## Changing an Array Element
This statement changes the value of the first element in cars:

```js
const cars = ["Saab", "Volvo", "BMW"];
cars[0] = "Opel";
```

## Array Multiple types
Array can hold multiple variable types

```js
const person = ["John", "Doe", 46, true, () => {}];
```

## The length Property
The length property of an array returns the length of an array (the number of array elements).

```js
const fruits = ["Banana", "Orange", "Apple", "Mango"];
let length = fruits.length;
```

## Accessing the Last Array Element
```js
const fruits = ["Banana", "Orange", "Apple", "Mango"];
let fruit = fruits[fruits.length - 1];
```

## Adding Array Elements
The easiest way to add a new element to an array is using the `push()` method:

```js
const fruits = ["Banana", "Orange", "Apple"];
fruits.push("Lemon");  // Adds a new element (Lemon) to fruits
```

## Array splice()
At position 2, add 2 elements:
```js
const fruits = ["Banana", "Orange", "Apple", "Mango"];

fruits.splice(2, 0, "Lemon", "Kiwi");
```

At position 2, remove 2 items:
```js
const fruits = ["Banana", "Orange", "Apple", "Mango", "Kiwi"];
fruits.splice(2, 2);
```
At position 2, add new items, and remove 1 item:

```js
const fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.splice(2, 1, "Lemon", "Kiwi");
```