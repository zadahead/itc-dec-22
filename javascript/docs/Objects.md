# Objects
A javaScript `object` is an entity having state and behavior (properties and method). For example: car, pen, bike, chair, glass, keyboard, monitor etc.

&nbsp;  

Here is an example of a JavaScript object.

```js
/ object
const student = {
    firstName: 'ram',
    class: 10
};
```

## Accessing Object Properties
You can access the `value` of a property by using its `key`.

```js
const person = { 
    name: 'John', 
    age: 20, 
};

// accessing property
console.log(person.name); // John
```
### Accessing Using bracket Notation
Here is the syntax of the bracket notation.

```js
console.log(person["name"]); // John
```

## JavaScript Nested Objects
An object can also contain another object. For example,

```js
// nested object
const student = { 
    name: 'John', 
    age: 20,
    marks: {
        science: 70,
        math: 75
    }
}

// accessing property of student object
console.log(student.marks); // {science: 70, math: 75}

// accessing property of marks object
console.log(student.marks.science); // 70
```

## JavaScript Object Methods
In JavaScript, an object can also contain a function. For example,

```js
const person = {
    name: 'Sam',
    age: 30,
    // using function as a value
    greet: function() { console.log('hello') }
}

person.greet(); // hello
```

