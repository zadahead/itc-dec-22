# Object Oriented Programming


lets create a game!
in this lesson we will develop a simple "Hit The Boxes!" game, build with OOP approach. 

## Step 1 - set up Element class
this class is about creating an element. we will pass "tag" in the `constructor` and it will create and element. 

```js
class Element {
    constructor(tag) {
        this.elem = this.createElement(tag);
    }

    createElement(tag) {
        const elem = document.createElement(tag);
        return elem;
    }
}

const box = new Element('div');
console.log(box)
```

## step 2 - add appenTo method
here we will add an `appendTo` feature so we will be able to append the element to the a wrapper

```html
<div class="canvas">

</div>
```

```js
class Element {
    constructor(tag) {
        this.elem = this.createElement(tag);
    }

    createElement(tag) {
        const elem = document.createElement(tag);
        return elem;
    }

    appendTo(selector = 'body') {
        const wrap = document.querySelector(selector)
        wrap.append(this.elem);
    }
}

const box = new Element('div');
box.appendTo('.canvas');
console.log(box);
```

## Step 3 - add more methods

now we can add more methods we can use later on like `write` so we can add text, or `addClass` so we can, well, add a class

```js
class Element {
    constructor(tag) {
        this.elem = this.createElement(tag);
    }

    createElement(tag) {
        const elem = document.createElement(tag);
        return elem;
    }

    appendTo(selector = 'body') {
        const wrap = document.querySelector(selector)
        wrap.append(this.elem);
    }

    write(text) {
        this.elem.innerHTML = text;
    }
}

const box = new Element('div');
box.appendTo('.canvas');

box.write('hello')
console.log(box);
```

### now lets add class

```css
.box {
    width: 50px;
    height: 50px;
    background-color: red;
    display: flex;
    justify-content: center;
    align-items: center;
}
```

```js
class Element {

    // ...

    addClass(cls) {
        this.elem.classList.add(cls);
    }
}
box.addClass('box');
```

## Step 4 - let add a Box class
so now that we see we have a specific element with a box features, maybe its a good call for us to create new class `Box` that will extend the features of the Element and add the default values that the box needs

```js
class Box extends Element {
    constructor(selector) {
        super('div')
        this.appendTo(selector);
        this.write('Im a box');
        this.addClass('box');
    }
}

const box = new Box('div');
```

## Step 5 - add new box onClick
now that we have a class for box, we can dynamically add the box with a click on the button

```css
.canvas {
    padding: 20px;
    display: flex;
    gap: 10px;
    flex-wrap: wrap;
}
```

```html
<button onclick="addBox()">Add Box</button>
<div class="canvas">

</div>
```

```js
const addBox = () => {
    new Box('.canvas');
}
```

### Step 6 - lets add the circle 
well, a circle is just like a box, only that it has a round edges and a different color, so lets extend our box class and make a circle out of it. 

also, we can create another button that will add the circle 

```css
.box.circle {
    border-radius: 50%;
    background-color: blue;
}
```
```html
<div>
    <button onclick="addBox()">Add Box</button>
    <button onclick="addCircle()">Add Circle</button>
</div>
<div class="canvas">

</div>
```

```js
class Circle extends Box {
    constructor(selector) {
        super(selector);
        this.addClass('circle');
        this.write('Im a circle');
    }
}
```

```js
const addCircle = () => {
    new Circle('.canvas');
}
```

## Step 7 now we can add events! 
so lets start with some static event. I want our Element class to statically receive  an object, and will bind an event to remove the element onClick

```js
class Element {
    
    // ...

    static removeOnClick(obj) {
        obj.elem.addEventListener('click', () => {
            obj.elem.remove();
        })
    }
}
```

and now lets bind it to the box 

```js
const addBox = () => {
    const box = new Box('.canvas');
    Element.removeOnClick(box);
}
```
### Step 8 - game setup
it order to start the game, we want to randomly add box or circle to the screen every second, 
and if we click on the square, it will be removed. 

```js
setInterval(() => {
    const rand = Math.floor(Math.random() * 10 + 1);
    if (rand > 5) {
        addBox();
    } else {
        addCircle();
    }
}, 1000);
```

### Step 9 - end the game 
now, we want to ability to stop the game at a certain point, so lets say that after 10 elements on screen, the game ended. we can do this like so 

```js
let timer;

timer = setInterval(() => {
    const boxes = document.querySelectorAll('.box');
    if (boxes.length == 10) {
        gameOver();
    } else {
        const rand = Math.floor(Math.random() * 10 + 1);
        if (rand > 5) {
            addBox();
        } else {
            addCircle();
        }
    }
}, 1000);

const gameOver = () => {
    clearInterval(timer);
}
```
`timer` is now a global object we can clear and set the interval on it
`gameOver` will clear the interval
each interval we will check for the `length` of the elements and if its bigger the 10, we will stop the game. 

## Step 10 - set up rules!
now we can set up the rules, if in the end of the game, all the elements are circles, it mean we have won! if at least one box left, it means we have lost :/
we can set this up on our `gameOver` function

```html
<div>
    <h1>Score: </h1>
</div>
<div class="canvas">

</div>
```
```js
const gameOver = () => {
    clearInterval(timer);

    const circles = document.querySelectorAll('.canvas .circle');

    const title = document.querySelector('h1');
    if (circles.length === 10) {
        title.innerHTML = "You Won!";
    } else {
        title.innerHTML = "You Lost :/";
    }
}
```

### Step 11 - More features! 
well now this is time to add more features, like styling for :hover and :active states
and also, maybe we can make the game harder by finish it if we click on a circle

```css
.box:hover {
    cursor: pointer;
    opacity: 0.8;
}

.box:active {
    opacity: 0.9;
}
```
```js
class Element {
    // ...

    onClick(func) {
        this.elem.addEventListener('click', func);
    }

}
```

```js
const addCircle = () => {
    const circle = new Circle('.canvas');
    circle.onClick(gameOver);
}
```
## Now everything together

```css
.box {
    width: 50px;
    height: 50px;
    background-color: red;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
}

.box:hover {
    cursor: pointer;
    opacity: 0.8;
}

.box:active {
    opacity: 0.9;
}

.box.circle {
    border-radius: 50%;
    background-color: blue;
}

.canvas {
    padding: 20px;
    display: flex;
    gap: 10px;
    flex-wrap: wrap;
}
```
```html
<div>
    <h1></h1>
</div>
<div class="canvas">

</div>
```
```js
class Element {
    constructor(tag) {
        this.elem = this.createElement(tag);
    }

    createElement(tag) {
        const elem = document.createElement(tag);
        return elem;
    }

    appendTo(selector = 'body') {
        const wrap = document.querySelector(selector)
        wrap.prepend(this.elem);
    }

    write(text) {
        this.elem.innerHTML = text;
    }

    addClass(cls) {
        this.elem.classList.add(cls);
    }

    onClick(func) {
        this.elem.addEventListener('click', func);
    }

    static removeOnClick(obj) {
        obj.elem.addEventListener('click', () => {
            obj.elem.remove();
        })
    }
}

class Box extends Element {
    constructor(selector) {
        super('div')
        this.appendTo(selector);
        this.addClass('box');
        this.write('Im a box');
    }
}

class Circle extends Box {
    constructor(selector) {
        super(selector);
        this.addClass('circle');
        this.write('Im a circle');
    }
}

const addBox = () => {
    const box = new Box('.canvas');
    Element.removeOnClick(box);
}

const addCircle = () => {
    const circle = new Circle('.canvas');
    circle.onClick(gameOver);
}

let timer;

timer = setInterval(() => {
    const elems = document.querySelectorAll('.box');
    if (elems.length == 10) {
        gameOver();
    } else {
        const rand = Math.floor(Math.random() * 10 + 1);
        if (rand > 5) {
            addBox();
        } else {
            addCircle();
        }
    }
}, 500);

const gameOver = () => {
    clearInterval(timer);

    const circles = document.querySelectorAll('.canvas .circle');

    const title = document.querySelector('h1');
    if (circles.length === 10) {
        title.innerHTML = "You Won!";
    } else {
        title.innerHTML = "You Lost :/";
    }
}
```