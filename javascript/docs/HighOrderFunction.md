
## High Order Function - (Custom Array Methods)
A “higher-order function” is a function that accepts functions as parameters and/or returns a function.


## Custom HOF
we can create our own high order functions, using prototypes.

#### .decor() custom high order function 
```js
Array.prototype.decor = function(oneach) {
    this.forEach((i, index) => {
        this[index] = oneach(i);
    })
}

const mosh = [1, 2, 3, 4];

mosh.decor((i) => {
    return 'dd' + i + 'dd';
})

console.log(mosh); ///['dd1dd', 'dd2dd', 'dd3dd', 'dd4dd']
```