# typeof

`typeof` command (a JavaScript reserved word) returns the type of the variables values as a string literal


```js
let a = 20;
typeof a; // 'number'
if (typeof a === 'number') {
   console.log('a is a number');
}
```

> Can return “string”, “number”, “boolean”, “undefined”, “object” & “function” 

&nbsp;  
## Array.isArray

if you check for the type of an Array or an Object, they both will give you the same `object` result. 
this is why, if you want to verify if the type is an Array, you can use `Array.isArray` function 

```js
const fruits = ["Banana", "Orange", "Apple", "Mango"];
let result = Array.isArray(fruits);
console.log(result); // true
```


