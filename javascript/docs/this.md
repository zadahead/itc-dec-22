# this

In JavaScript, the `this` keyword refers to an object. Which object depends on how this is being invoked (used or called)

> basic this is the window object

```js 
const func = () => {
    console.log(this); //Window object
}

func();
```

> this in functions vs this in arrow functions

```js
window.name = 'david';

const obj = {
    age: 25,
    name: 'mosh',

    arrowName: () => {
        console.log(this.name);
    },
    funcName: function () {
        console.log(this.name);
    }
}

obj.arrowName(); //david
obj.funcName(); //mosh
```

> this in classes 

```js
 class Cls {
    static baseName = 'Baruch';

    constructor(name) {
        this.name = name;
    }

    logNameArrow = () => {
        console.log(this.name);
    }

    logName () {
        console.log(this.name);
    }

    static staticLog = () => {
        console.log(this.baseName);
    }
}

const cls = new Cls('Mosh');
cls.logNameArrow(); //logs Mosh

const cls2 = new Cls('David');
cls2.logName(); //logs David

Cls.staticLog(); //logs Baruch
```

> this in HTML elements
```js
const handleClick = () => {
    console.log(this);
}

document.querySelector('h1').addEventListener('click', handleClick); //logs Window object


handleClick(); //logs Window object

const experiment = {
    tryclick: handleClick
}

experiment.tryclick(); //logs Window object
```

the same with regular functions instead of arrow functions 

```js
const handleClick = function (){
    console.log(this);
}

document.querySelector('h1').addEventListener('click', handleClick); //logs HTML Element


handleClick(); //logs Window object

const experiment = {
    tryclick: handleClick
}

experiment.tryclick(); //logs tryclick function
```


> bind and call
```js
const lastObject = {
    name: 'mosh',

    getName: function (x) {
        console.log(this.name, x);
    }
}

const anotherObject = {
    name: 'david'
}

lastObject.getName(1); //logs mosh 1

const binded = lastObject.getName.bind(anotherObject); 
binded(2); //logs david 2

lastObject.getName.call(anotherObject, [1, 2, 3]); //logs david [1, 2, 3]
```