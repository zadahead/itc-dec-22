# Error handling

No matter how great we are at programming, sometimes our scripts have errors. They may occur because of our mistakes, an unexpected user input, an erroneous server response, and for a thousand other reasons.

&nbsp;  


Usually, a script “dies” (immediately stops) in case of an error, printing it to console.

&nbsp;  

But there’s a syntax construct `try...catch` that allows us to “catch” errors so the script can, instead of dying, do something more reasonable.


## The “try…catch” syntax
The `try...catch` construct has two main blocks: try, and then catch:

```js
try {

  // code...

} catch (err) {

  // error handling

}
```

It works like this:

1. First, the code in `try {...}` is executed.
1. If there were no errors, then `catch (err)` is ignored: the execution reaches the end of `try` and goes on, skipping catch.
1. If an error occurs, then the `try` execution is stopped, and control flows to the beginning of `catch (err)`. The err variable (we can use any name for it) will contain an error object with details about what happened.


Let’s look at some examples.

_An errorless example: shows alert (1) and (2):_
```js
try {

  alert('Start of try runs');  // (1) <--

  // ...no errors here

  alert('End of try runs');   // (2) <--

} catch (err) {

  alert('Catch is ignored, because there are no errors'); // (3)

}
```
_An example with an error: shows (1) and (3):_

```js
try {

  alert('Start of try runs');  // (1) <--

  lalala; // error, variable is not defined!

  alert('End of try (never reached)');  // (2)

} catch (err) {

  alert(`Error has occurred!`); // (3) <--

}
```

## try...catch works synchronously

If an exception happens in “scheduled” code, like in setTimeout, then try...catch won’t catch it:

```js
try {
  setTimeout(function() {
    noSuchVariable; // script will die here
  }, 1000);
} catch (err) {
  alert( "won't work" );
}
```

That’s because the function itself is executed later, when the engine has already left the try...catch construct.

To catch an exception inside a scheduled function, try...catch must be inside that function:

```js
setTimeout(function() {
  try {
    noSuchVariable; // try...catch handles the error!
  } catch {
    alert( "error is caught here!" );
  }
}, 1000);
```

## Error object

When an error occurs, JavaScript generates an object containing the details about it. The object is then passed as an argument to `catch`:

```js
try {
  // ...
} catch (err) { // <-- the "error object", could use another word instead of err
  // ...
}
```
For all built-in errors, the error object has two main properties:

### name
Error name. For instance, for an undefined variable that’s "ReferenceError".

### message
Textual message about error details.
There are other non-standard properties available in most environments. One of most widely used and supported is:

### stack
Current call stack: a string with information about the sequence of nested calls that led to the error. Used for debugging purposes.

```js
try {
  lalala; // error, variable is not defined!
} catch (err) {
  alert(err.name); // ReferenceError
  alert(err.message); // lalala is not defined
  alert(err.stack); // ReferenceError: lalala is not defined at (...call stack)

  // Can also show an error as a whole
  // The error is converted to string as "name: message"
  alert(err); // ReferenceError: lalala is not defined
}
```

## “Throw” operator

The throw operator generates an error.

```js
let json = '{ "age": 30 }'; // incomplete data
try {

  let user = JSON.parse(json);

  if (!user.name) {
    throw new SyntaxError("Incomplete data: no name");
  }

  blabla(); // unexpected error

  alert( user.name );

} catch (err) {

  if (err instanceof SyntaxError) {
    alert( "JSON Error: " + err.message );
  } else {
    throw err; // rethrow (*)
  }

}
```

## try…catch…finally

Wait, that’s not all.

The `try...catch` construct may have one more code clause: `finally`.

If it exists, it runs in all cases:

1. after try, if there were no errors,
1. after catch, if there were errors.

```js
try {
  alert( 'try' );
  if (confirm('Make an error?')) BAD_CODE();
} catch (err) {
  alert( 'catch' );
} finally {
  alert( 'finally' );
}
```

## Global catch

Let’s imagine we’ve got a fatal error outside of try...catch, and the script died. Like a programming error or some other terrible thing.

Is there a way to react on such occurrences? We may want to log the error, show something to the user (normally they don’t see error messages), etc.

```js
window.onerror = function(message, url, line, col, error) {
  // ...
};
```

&nbsp;  


resource: https://javascript.info/try-catch

