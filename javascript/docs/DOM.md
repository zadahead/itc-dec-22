# Introduction to the DOM

The Document Object Model (`DOM`) is the data representation of the objects that comprise the structure and content of a document on the web. This guide will introduce the DOM, look at how the DOM represents an HTML document in memory and how to use basic APIs to interact with web content and applications.

```html
<html>
  <body>
      <div>
          <h1>This is a child of a child</h1>
      </div>
  </body>
</html>
```
we can access `h1` tag with the DOM object, like so:
```js
document.body.children[0].children[0]; //this will give us h1 tag.
```

## Core interfaces in the DOM

This section lists some of the most commonly-used interfaces in the DOM. The idea is not to describe what these APIs do here but to give you an idea of the sorts of methods and properties you will see very often as you use the DOM. These common APIs are used in the longer examples in the DOM Examples chapter at the end of this book.

### querySelector
The Document method `querySelector()` returns the first Element within the document that matches the specified selector, or group of selectors. If no matches are found, null is returned.

```html
<div id="foo"></div>
<div class="bar"></div>

<script>
  document.querySelector("#foo"); // Match the first div
  document.querySelector(".bar"); // Match the second div
</script>
```

### querySelectorAll
The Document method `querySelectorAll()` returns a static (not live) NodeList representing a list of the document's elements that match the specified group of selectors.
```html
<div id="foo"></div>
<div class="bar"></div>

<script>
  document.querySelectorAll("#foo"); // Match array with the first div as the first item
  document.querySelectorAll("div"); // Match both div's
</script>
```
### Element.innerHTML
The Element property `innerHTML` gets or sets the HTML or XML markup contained within the element.

```html
<div class="bar">Hello</div>

<script>
  document.querySelector(".bar").innerHTML = 'Goodbye'; //div will be set with a new value
</script>
```
### createElement
In an HTML document, the document.createElement() method creates the HTML element specified by tagName.
```html
<div class="bar">Hello</div>

<script>
  const newDiv = document.createElement("div"); 
  //this will create a new div.
  //it WILL NOT append the element to the page.
</script>
```
### appendChild 
The `appendChild()` method of the Node interface adds a node to the end of the list of children of a specified parent node. 

> If the given child is a reference to an existing node in the document, appendChild() moves it from its current position to the new position.

```html
<div class="bar"></div>

<script>
  const newDiv = document.createElement("div"); 

  newDiv.innerHTML = "My brand new Div";

  document.querySelector(".bar").appendChild(newDiv);
  //this will create a new div.
  //it will set its value
  //it will append the div to .bar tag
</script>
```

### HTMLElement.style
The `element.style` property returns the inline style of an element in the form of an object that contains a list of all styles properties for that element 

to add specific styles to an element without altering other style values:

```html
<div class="bar">Hello Mosh</div>

<script>
  document.querySelector(".bar").style.color = 'red';
  //this will set the color to red
</script>
```

### setAttribute
To set the value of an attribute on the specified element, use `setAttribute()`. If the attribute already exists, the value is updated; otherwise a new attribute is added with the specified name and value.

```html
<div class="bar" data-count="0">Hello Mosh</div>

<script>
  document.querySelector(".bar").setAttribute('data-count', '1');
  //"data-count" will be updated to "1"
</script>
```

### getAttribute
The `getAttribute()` method of the Element interface returns the value of a specified attribute on the element.

```html
<div class="bar" data-count="0">Hello Mosh</div>

<script>
  document.querySelector(".bar").getAttribute('data-count');
  //will return "0"
</script>
```

### removeAttribute
The Element method `removeAttribute()` removes the attribute with the specified name from the element.
```html
<div class="bar" data-count="0">Hello Mosh</div>

<script>
  document.querySelector(".bar").removeAttribute('data-count',);
  //will remove data-count attribute
</script>
```