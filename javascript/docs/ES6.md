# ES6 

Some cool new features added to Javascript world,
and these are widely in use in modern development and this is critical for us to  be familiar with the syntax and usage  

## The Spread (...) Operator

The `...` operator expands an iterable (like an array) into more elements:

```js
const cars1 = ["Saab", "Volvo", "BMW"];
const cars2 = ["Saab", "Volvo", [..."BMW"]];
```
The ... operator can be used to expand an iterable into more arguments for function calls:

```js 
const numbers = [23,55,21,87,56];
let maxValue = Math.max(...numbers);
```

## Default Parameter Values
ES6 allows function parameters to have default values.

```js
function myFunction(x, y = 10) {
  // y is 10 if not passed or undefined
  return x + y;
}
myFunction(5); // will return 15
```

## Function Rest Parameter
The rest parameter (...) allows a function to treat an indefinite number of arguments as an array:

```js
function sum(...args) {
  let sum = 0;
  for (let arg of args) sum += arg;
  return sum;
}

let x = sum(4, 9, 16, 25, 29, 100, 66, 77);
```

## Destructuring
Destructuring makes it easy to extract only what is needed.

```js
const vehicles = ['mustang', 'f-150', 'expedition'];

const [car, truck, suv] = vehicles;
```

```js
const vehicleOne = {
  brand: 'Ford',
  model: 'Mustang',
  type: 'car',
  year: 2021, 
  color: 'red'
}

const {type, color, brand, model} = vehicleOne;

```

## The object literal property value shorthand
long title for a simple solution, It allows us to define an object whose keys have the same names as the variables passed in as properties by simply passing the variables:

before: 
```js
var name = 'John Doe'
var email = 'john.doe@example.com'
var age = 25

var user = {
  name: name,
  email: email,
  age: age
}
```

after: 
```js
let name = 'John Doe'
let email = 'john.doe@example.com'
let age = 25

let user = { name, email, age }
```

## Sets

A JavaScript Set is a collection of unique values.
Each value can only occur once in a Set.
A Set can hold any value of any data type.

### The new Set() Method
```js
const letters = new Set(["a","b","c"]);
```

### Add to a set
```js
// Create a Set
const letters = new Set();

// Add Values to the Set
letters.add("a");
letters.add("b");
letters.add("c");
```

### Check it item exist 
```js
letters.has('d'); //false
letters.has('c'); //true
```

### forEach Set

```js
// Create a Set
const letters = new Set(["a","b","c"]);

// List all entries
let text = "";
letters.forEach (function(value) {
  text += value;
})
```


resource: https://www.w3schools.com/js/js_es6.asp