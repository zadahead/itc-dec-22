# Mutables

## Primitive vs Non-Primitive Value

If you are interviewing for a frontend developer role, there is a high chance that you may be asked this question.  

&nbsp;  

So what exactly are these?  

&nbsp;  

To understand that we need to know how many different types of data type present in Javascript.

1. String
1. Boolean
1. Number
1. BigInt
1. Null
1. Undefined
1. Symbol
1. Object (include Array, cos this is an object) 

Now, these data types are broadly classified into 2 types:

*Primitive:* 
 - String
 - Boolean
 - Number
 - BigInt
 - Null
 - Undefined
 - Symbol

*Non-Primitive:*
- functions 
- Object (array, also called an object)
  
&nbsp;  
The fundamental difference between `primitives` and `non-primitive` is  

&nbsp;  
 
primitives are `immutable` 
non-primitive are `mutable`  

&nbsp;  

*Mutable* values are those which can be modified after creation.
&nbsp;  
*Immutable* values are those which cannot be modified after creation.


### Primitives
Primitives are known as being `immutable` data types because there is no way to change a primitive value once it gets created.

```js
let string = 'Hello to you';
string = 'Hello Mosh';
console.log(string); //-> Hello Mosh;
```

It’s important to note in the above example that a variable that stored primitive value can be reassigned to a new value as shown in the above example but the existing value can’t be changed as shown below:-

```js
let string = 'Hello to you';
string[0] = 'M';
console.log(string); //-> Hello to you;
```
&nbsp;  
Second, Primitive is compared by value.  
Two values are strictly equal if they have the same value.

```js
const example1 = 'This is a test string';
const example2 = 'This is a test string';
example1 == example2 // True
```

### Non-Primitives
Non-Primitives are known as `mutable` data types because we can change the value after creation.

```js
let arr =  [1, 2, 3, 4, 5, 6];
arr[2] = 5;
console.log(arr); //-> [1, 2, 5, 4, 5, 6];
```

As you can see in the above example we can change the array after creation.

&nbsp;  

Secondly, Objects are not compared by value, they are being compared by reference.

&nbsp;  

For example, if two objects have the same key-value pair, they are not strictly equal. The same goes for arrays. Even if they have the same elements that are in the same order, they are not strictly equal.

```js
let arr =  [1, 2, 3];
let arr2 =  [1, 2, 3];

console.log(arr === arr2); //-> false;

let obj =  { id: 1, name: 'mosh' };
let obj2 =  { id: 1, name: 'mosh' };

console.log(obj === obj2); //-> false;

```

&nbsp;  

Two objects are only strictly equal if they refer to the same underlying object.

&nbsp;  

Example:-

```js
let obj1 = {name: 'test', city: 'Jaipur'}
let obj3 = obj1;
console.log(obj1 === obj3); // true
```
