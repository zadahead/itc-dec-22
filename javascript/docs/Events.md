# Events

An HTML event can be something the browser does, or something a user does.

Here are some examples of HTML events:

1. An HTML web page has finished loading
1. An HTML input field was changed
1. An HTML button was clicked

Often, when events happen, you may want to do something.
JavaScript lets you execute code when events are detected.

```html
<button onclick="logThis()">Log</button>

<script>
function logThis() {
   console.log('This is a log');
}
</script>
```

### addEventListener
The `addEventListener()` method of the EventTarget interface sets up a function that will be called whenever the specified event is delivered to the target.

```html
<table id="outside">
  <tr>
    <td id="t1">one</td>
  </tr>
  <tr>
    <td id="t2">two</td>
  </tr>
</table>

<script>
// Function to change the content of t2
function modifyText() {
  var t2 = document.querySelector("#t2");
  var isThree = t2.innerHTML === "three";
  t2.innerHTML = isThree ? "two" : "three";
}

// Add event listener to table
const el = document.querySelector("#outside");
el.addEventListener("click", modifyText);
</script>
```