# Classes

Definition: a number of persons or things regarded as forming a group by reason of common attributes, characteristics, qualities, or traits; kind; sort:

Classes are a template for creating objects. They encapsulate data with code to work on that data. 

ES6 introduced a new syntax for declaring a class as shown in this example:

## ES6 class declaration
```js
class Person {
    constructor(name) {
        this.name = name;
    }
    getName() {
        return this.name;
    }
}
```

The following creates a new Person object, which will automatically call the `constructor()` of the Person class:

```js
let john = new Person("John Doe");
```

The getName() is called a method of the Person class. Like a constructor function, you can call the methods of a class using the following syntax:

```js
let name = john.getName();
console.log(name); // "John Doe"
```

A class element can be characterized by three aspects:

1. Kind: Getter, setter, method, or field
1. Location: Static or instance
1. Visibility: Public or private

### Method definitions
Method definition is a shorter syntax for defining a function property in an object initializer.

```js
class ClassWithPublicInstanceMethod {
  publicMethod() {
    return "hello world";
  }
}

const instance = new ClassWithPublicInstanceMethod();
console.log(instance.publicMethod()); // "hello world"
```

### getter
The get syntax binds an object property to a function that will be called when that property is looked up

```js
class ClassWithGet {
  #msg = "hello world";
  get msg() {
    return this.#msg;
  }
}

const instance = new ClassWithGet();
console.log(instance.msg); // "hello world"
```
### setter
The set syntax binds an object property to a function to be called when there is an attempt to set that property

```js
class ClassWithGetSet {
  #msg = "hello world";
  get msg() {
    return this.#msg;
  }
  set msg(x) {
    this.#msg = `hello ${x}`;
  }
}

const instance = new ClassWithGetSet();
console.log(instance.msg); // "hello world"

instance.msg = "cake";
console.log(instance.msg); // "hello cake"
```
### Public class fields - inheritance
Both static and instance public fields are writable, enumerable, and configurable properties. As such, unlike their private counterparts, they participate in prototype inheritance.

```js
class Base {
  baseField = "base field";
  anotherBaseField = this.baseField;
  baseMethod() {
    return "base method output";
  }
}

class Derived extends Base {
  subField = super.baseMethod();
}

const base = new Base();
const sub = new Derived();

console.log(base.anotherBaseField); // "base field"

console.log(sub.subField); // "base method output"
```

### static
The static keyword defines a static method or field for a class, or a static initialization block (see the link for more information about this usage). Static properties cannot be directly accessed on instances of the class. Instead, they're accessed on the class itself.

```js
class ClassWithStaticMethod {
  static staticProperty = 'someValue';
  static staticMethod() {
    return 'static method has been called.';
  }
  static {
    console.log('Class static initialization block called');
  }
}

console.log(ClassWithStaticMethod.staticProperty);
// Expected output: "someValue"
console.log(ClassWithStaticMethod.staticMethod());
// Expected output: "static method has been called."
```

### Private class features
Class fields are public by default, but private class members can be created by using a hash # prefix. The privacy encapsulation of these class features is enforced by JavaScript itself.

```js
class ClassWithPrivateMethod {
  #privateMethod() {
    return "hello world";
  }

  getPrivateMessage() {
    return this.#privateMethod();
  }
}

const instance = new ClassWithPrivateMethod();
console.log(instance.getPrivateMessage());
// hello world
```

### Overriding methods
Overriding a method replaces the code of the method in the superclass with that of the subclass.

```js
class Human {
 constructor(weapon) {
  this.weapon = weapon;
  this.health = 100;
 }
 receiveDamage() {
  this.health = this.health - 10;
 }
}
class Wizard extends Human {
 receiveDamage() {
  this.health = this.health - 5;
 }
}
const wizard = new Wizard("staff");
console.log(wizard.health);
wizard.receiveDamage();
console.log(wizard.health);
```
resource: 
https://www.javascripttutorial.net/es6/javascript-class/  
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes