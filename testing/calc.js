const add = (a, b) => {
    return a + b;
}

const mult = (a, b) => {
    if (!a || !b) {
        throw new Error("not a valid args")
    }
    return a * b;
}

module.exports = {
    add,
    mult
}