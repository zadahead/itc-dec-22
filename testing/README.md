## to install

```cmd
npm install --save-dev babel-cli babel-preset-env jest
```

### to add intellisense

jsconfig.json

```json
{
    "typeAcquisition": {
        "include": [
            "jest"
        ]
    }
}
```

```cmd
npm install @types/jest
```