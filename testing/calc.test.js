const { add, mult } = require("./calc");

describe("Calc functionallity", () => {

    describe("testing add", () => {
        it('should be true', () => {
            expect(true).toBe(true);
        })

        it('should be result 22', () => {
            const result = add(10, 12);

            expect(result).toEqual(22);
        })
    })

    describe("testing mult", () => {
        it('will validate multiple ', () => {
            const result = mult(2, 10);
            const result2 = mult(5, 10);

            expect(result).toEqual(20);
            expect(result2).toEqual(50);
        })

        it('will call multiple with no args', () => {
            const errorWrap = () => {
                mult();
            }
            expect(errorWrap).toThrow('not a valid args');
        })
    })




    //test calc
    //test mult

})

