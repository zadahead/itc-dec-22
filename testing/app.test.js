const app = require('./app');

const request = require('supertest');


describe('Testing /users end-to-end', () => {
    /*
        1) make sure, list is empty
        2) add user to the list
        3) update the user
        4) get the user by id, and make sure it is updated
        5) delete the user
        6) make sure list is empty
    */
    describe('Text complete CRUD', () => {
        let newId = '';

        it('will GET /users, empty list', async () => {
            return request(app)
                .get('/users')
                .then(resp => {
                    expect(resp.body).toEqual([])
                })
        })

        it('will POST /users, add new user', async () => {
            return request(app)
                .post('/users')
                .send({ name: 'mosh' })
                .then(resp => {
                    console.log(resp.body);
                    expect(resp.body.newId).not.toBeUndefined();
                    expect(resp.statusCode).toBe(201);

                    newId = resp.body.newId;
                })
        })

        it('will GET /users/:userId. find new user by id. ', () => {
            return request(app)
                .get(`/users/${newId}`)
                .then(resp => {
                    console.log(resp.body);
                    expect(resp.body.id).toEqual(newId);
                    expect(resp.body).toEqual({
                        id: newId,
                        name: 'mosh'
                    })
                })
        })



        it('will PUT /users/:userId. and update user', () => {
            return request(app)
                .put(`/users/${newId}`)
                .send({ name: 'mosh11' })
                .then(resp => {
                    expect(resp.text).toBe('success')
                    expect(resp.statusCode).toBe(200);
                })
        })

        it('will GET /users/:userId. find new user by id. ', () => {
            return request(app)
                .get(`/users/${newId}`)
                .then(resp => {
                    expect(resp.body).toEqual(
                        expect.objectContaining({
                            name: 'mosh11'
                        })
                    )
                })
        })

        it('will DELETE /users/:userId. delete the user', () => {
            return request(app)
                .delete(`/users/${newId}`)
                .then(resp => {
                    expect(resp.statusCode).toBe(200);
                    expect(resp.text).toBe('success');
                })
        })

        it('make sure its empty', () => {
            return request(app)
                .get(`/users/${newId}`)
                .then(resp => {
                    expect(resp.statusCode).toBe(404);
                })
        })
    })


    describe('test failing requests', () => {
        it('will failed POST /users without req.body', async () => {
            return request(app)
                .post('/users')
                .then((resp) => {
                    expect(resp.text).toBe('error');
                    expect(resp.statusCode).toBe(400);
                })
        })

        it('will failed POST /users without req.body.name', async () => {
            return request(app)
                .post('/users')
                .send({ age: 26 })
                .then((resp) => {
                    expect(resp.text).toBe('error');
                    expect(resp.statusCode).toBe(400);
                })
        })
    })



})