const usersController = require("./users.controller")
const usersServices = require('../services/users.service');

const request = {

}

const response = {
    status() {
        return this;
    },
    send() {
        return this;
    }
}

const responseSendSpy = jest.spyOn(response, 'send');
const responseStatusSpy = jest.spyOn(response, 'status');

const getAllUsersSpy = jest.spyOn(usersServices, 'getAllUsers');

describe('Test users.controller', () => {
    it('will validate getAllUsers', () => {
        getAllUsersSpy.mockReturnValue([
            { id: 1, name: 'mosh' }
        ])

        usersController.getAllUsers(request, response);
        expect(responseSendSpy).toHaveBeenCalled();
        expect(responseSendSpy).toBeCalledTimes(1);

        expect(responseStatusSpy).toHaveBeenCalledWith(200);

        expect(responseSendSpy).toHaveBeenCalledWith([
            { id: 1, name: 'mosh' }
        ]);
    })
})