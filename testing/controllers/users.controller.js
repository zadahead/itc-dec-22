const usersService = require('../services/users.service');

module.exports.getAllUsers = (req, res) => {
    res.status(200).send(usersService.getAllUsers());
}

module.exports.getUserById = (req, res) => {
    const { userId } = req.params;

    const user = usersService.findUserById(userId);

    if (!user) {
        return res.status(404).send('user not exist');
    }

    res.status(200).send(user);
}

module.exports.addNewUser = (req, res) => {
    if (!req.body.name) {
        return res.status(400).send('error');
    }

    const newId = usersService.addNewUser(req.body);

    return res.status(201).send({ newId });
}


module.exports.updateUser = (req, res) => {
    const { userId } = req.params;

    const index = usersService.findUserIndexById(userId);

    if (index < 0) {
        return res.status(404).send('user not exist');
    }

    usersService.updateUserByIndex(index, userId, req.body);

    return res.status(200).send('success');
}


module.exports.deleteUser = (req, res) => {
    const { userId } = req.params;

    const index = usersService.findUserIndexById(userId);

    usersService.deleteUserByIndex(index);

    res.status(200).send('success');
}