const crypto = require('crypto');

const users = [
];

module.exports.getAllUsers = () => {
    return users;
}

module.exports.findUserById = (userId) => {
    return users.find(u => u.id === userId);
}

module.exports.findUserIndexById = (userId) => {
    return users.findIndex(u => u.id === userId);
}

module.exports.updateUserByIndex = (index, userId, user) => {
    users[index] = {
        ...user,
        id: userId,
    }
}

module.exports.deleteUserByIndex = (index) => {
    users.splice(index, 1);
}

module.exports.addNewUser = (user) => {
    const newId = crypto.randomUUID();

    users.push({
        id: newId,
        ...user
    });

    return newId;
}