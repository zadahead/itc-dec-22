const express = require('express');
const app = express();

const usersController = require('./controllers/users.controller');

app.use(express.json());



app.get('/test', (req, res) => {
    res.status(200).send('success');
})

app.get('/users', usersController.getAllUsers);

app.post('/users', usersController.addNewUser);

app.get('/users/:userId', usersController.getUserById);

app.put('/users/:userId', usersController.updateUser);

app.delete('/users/:userId', usersController.deleteUser);

//post
// valid req -> { name: 'david' }
// if valid, response with 201
// if wrong, response with 400

module.exports = app;

