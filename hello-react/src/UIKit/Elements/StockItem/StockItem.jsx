import { Between, Rows, Btn, Icon, Line } from "UIKit"
import './StockItem.css';

export const StockItem = ({ children, onDelete, onEdit }) => {
    return (
        <div className="StockItem">
            <Between>
                {children}
                <Rows>
                    <Line>
                        <Icon i="delete" onClick={onDelete} />
                        <Icon i="edit" onClick={onEdit} />
                    </Line>
                </Rows>
            </Between>
        </div>
    )
}

export const ListItem = ({ children }) => {
    return (
        <div className="ListItem">
            <Rows>
                {children}
            </Rows>
        </div>
    )
}