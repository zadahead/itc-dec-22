import './Btn.css';

import { Center, Icon } from 'UIKit';

export const Btn = (props) => {
    return (
        <button className='Btn' onClick={props.onClick}>
            <Center>
                {props.i && <Icon i={props.i} />}
                {props.children}
            </Center>
        </button>
    )
}

export const BtnSecond = (props) => {
    return (
        <button className='Btn' onClick={props.onClick}>
            <Center>
                {props.i && <Icon i={props.i} />}
                {props.children}
                second
            </Center>
        </button>
    )
}


export const LinkBtn = (props) => {
    return (
        <button className='LinkBtn' onClick={props.onClick}>
            <Center>
                {props.i && <Icon i={props.i} />}
                {props.children}
            </Center>
        </button>
    )
}

export default Btn;