import './Msg.css';

export const Msg = ({ msg, isError }) => {
    return (
        <div className="Msg" data-error={isError}>
            <h3>{msg}</h3>
        </div>
    )
}