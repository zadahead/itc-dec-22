import './Icon.css';

export const Icon = (props) => {
    return (
        <span
            className="Icon material-symbols-rounded"
            data-clickable={props.onClick && true}
            onClick={props.onClick}
        >
            {props.i || props.children || 'no-icon'}
        </span>
    )
}

export default Icon;