import { Icon, Line, Rows } from 'UIKit';
import './Radiobtn.css';

export const Radiobtn = ({ children, selected, onChange }) => {
    return (
        <div className="Radiobtn" onClick={onChange}>
            <Line>
                <Icon i={selected ? 'radio_button_checked' : 'radio_button_unchecked'} />
                <h3>{children}</h3>
            </Line>
        </div>
    )
}

export const Radiobtns = ({ list, selected, onChange }) => {

    const isSelected = (i) => {
        return i.id === selected;
    }

    const handleChange = (i) => {
        onChange(i.id);
    }

    const renderList = () => {
        return list.map(i => {
            return (
                <Radiobtn
                    key={i.id}
                    selected={isSelected(i)}
                    onChange={() => handleChange(i)}
                >
                    {i.title}
                </Radiobtn>
            )
        })
    }
    return (
        <Rows>
            {renderList()}
        </Rows>
    )
}