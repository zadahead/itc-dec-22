import './Loader.css';
import SpinnerSrc from 'Assets/spinner.svg';


export const Loader = () => {
    return (
        <img className='Loader' src={SpinnerSrc} alt="Spinner" />
    )
}