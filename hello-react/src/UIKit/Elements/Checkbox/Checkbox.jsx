import { Icon, Line, Rows } from "UIKit"
import './Checkbox.css';
import { useState } from "react";

export const Checkbox = ({ checked, onChange, children }) => {


    const handleToggle = () => {
        onChange(!checked);
    }

    return (
        <div className="Checkbox" onClick={handleToggle}>
            <Line>
                <Icon i={checked ? 'check_box' : 'check_box_outline_blank'} />
                <h3>{children}</h3>
            </Line>
        </div>
    )
}
/*
const list = [
    { 
        id: 'hide_network',
        title: 'Hide network',
        checked: true
    },
    {
        id: 'preserve_log',
        title: 'Preserve log',
        checked: false
    }
]
*/
export const Checkboxes = ({ list, onChange }) => {

    const handleChange = (item) => {
        item.checked = !item.checked;
        onChange([...list]);
    }

    const renderList = () => {
        return list.map(i => {
            return (
                <Checkbox
                    key={i.id}
                    checked={i.checked}
                    onChange={() => handleChange(i)}
                >
                    {i.title}
                </Checkbox>
            )
        })
    }
    return (
        <Rows>
            {
                renderList()
            }
        </Rows>
    )
}