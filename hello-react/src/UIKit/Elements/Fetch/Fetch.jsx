const { useFetch } = require("Hooks/useFetch");
const { Loader } = require("../Loader/Loader");

export const Fetch = ({ url }) => {
    const { isLoading, list, error } = useFetch(url);
    console.log(error);

    //render
    const renderList = () => {
        if (error) {
            const styleCss = {
                color: 'red'
            }
            return <h3 style={styleCss}>{error}</h3>
        }

        if (isLoading) {
            return <Loader />
        }

        return list.map(i => {
            return <h4 key={i.id}>{i.name || i.title}</h4>
        })
    }

    return (
        <>
            {renderList()}
        </>
    );
}