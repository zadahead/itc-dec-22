import { Between, Icon } from 'UIKit';
import './Dropdown.css';
import { useEffect, useState } from 'react';

export const Dropdown = ({ list, selected, onChange }) => {
    const [isDisp, setIsDisp] = useState(false);

    useEffect(() => {
        document.body.addEventListener('click', handleBodyClick);

        return () => {
            document.body.removeEventListener('click', handleBodyClick);
        }
    }, [])

    const handleBodyClick = () => {
        setIsDisp(false);
    }

    const handleToggle = (e) => {
        e.stopPropagation();
        setIsDisp(!isDisp);
    }

    const renderHeader = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);

            if (item) {
                return item.value;
            }
        }
        return "Please Select...";
    }

    const handleSelect = (item) => {
        console.log('handleSelect', item)
        onChange(item.id);
        setIsDisp(false);
    }

    const renderList = () => {
        return list.map(item => {
            return (
                <h4
                    className={item.id === selected ? 'selected' : ''}
                    key={item.id}
                    onClick={() => handleSelect(item)}
                >
                    {item.value}
                </h4>
            )
        })
    }

    return (
        <div className='Dropdown'>
            <div className='header' onClick={handleToggle}>
                <Between>
                    <h4>{renderHeader()}</h4>

                    <Icon i="expand_more" />
                </Between>
            </div>
            {
                isDisp && (
                    <div className='list'>
                        {renderList()}
                    </div>
                )
            }
        </div>
    )
}