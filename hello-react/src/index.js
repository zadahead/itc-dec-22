import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';

import { BrowserRouter } from 'react-router-dom';

import App from './App';
import { CountProvider } from 'Context/countContext';
import { ColorSwitchProvider } from 'Context/colorSwitchContext';
import { TodosProvider } from 'Context/todosContext';
import { AuthProvider } from 'Auth/authContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
        <AuthProvider>
            <TodosProvider>
                <ColorSwitchProvider>
                    <CountProvider>
                        <App />
                    </CountProvider>
                </ColorSwitchProvider>
            </TodosProvider>
        </AuthProvider>
    </BrowserRouter>
);


