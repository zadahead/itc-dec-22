const USER_ID = 'user-id';

export const getStorageUser = () => {
    return localStorage.getItem(USER_ID);
}

export const getStorageIsLogin = () => {
    return !!getStorageUser();
}

export const clearStorageUser = () => {
    localStorage.removeItem(USER_ID);
}

export const setStorageUser = (value) => {
    localStorage.setItem(USER_ID, value);
}