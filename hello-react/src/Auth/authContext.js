import { createContext, useState } from "react";
import { clearStorageUser, getStorageIsLogin, setStorageUser } from "./storage";
import { useNavigate } from "react-router-dom";

export const authContext = createContext({});

const Provider = authContext.Provider;


export const AuthProvider = ({ children }) => {
    const [isLogin, setIsLogin] = useState(getStorageIsLogin());
    const navigate = useNavigate();

    const login = (userId) => {
        setIsLogin(true);
        setStorageUser(userId);
        navigate('/');
    }

    const logout = () => {
        setIsLogin(false);
        clearStorageUser();
        navigate('/login');
    }

    const value = {
        isLogin,
        logout,
        login
    }


    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}
