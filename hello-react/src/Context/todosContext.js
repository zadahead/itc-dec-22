import { createContext, useEffect, useState } from "react";

const TODOS = 'todos-list';

const getLocalTodos = () => {
    const todos = localStorage.getItem(TODOS);
    if (todos) {
        return JSON.parse(todos);
    }
    return []
}

export const todosContext = createContext({});

const Provider = todosContext.Provider;

export const TodosProvider = ({ children }) => {
    const [list, setList] = useState(getLocalTodos());
    const [value, setValue] = useState('');

    const notCompleted = list.filter(i => !i.completed).length;


    useEffect(() => {
        localStorage.setItem(TODOS, JSON.stringify(list));
    }, [list])

    const handleDelete = (item) => {
        const filteredList = list.filter(i => i !== item);
        setList(filteredList);

    }

    const handleAdd = () => {
        if (!value) { return }

        const newList = [
            ...list,
            {
                id: crypto.randomUUID(),
                title: value,
                completed: false
            }
        ]

        setList(newList);
        setValue('');
    }

    const handleToggle = (item) => {
        item.completed = !item.completed;
        setList([...list]);
    }

    const providerValue = {
        list,
        value,
        setValue,
        handleAdd,
        handleToggle,
        handleDelete,
        notCompleted
    }
    return (
        <Provider value={providerValue}>
            {children}
        </Provider>
    )
}