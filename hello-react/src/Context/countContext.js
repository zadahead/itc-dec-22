import { createContext, useState } from "react";


export const countContext = createContext({});

const Provider = countContext.Provider;

export const CountProvider = ({ children }) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1)
    }

    const value = {
        count,
        handleAdd,
        setCount,
        name: 'mosh'
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}