import { createContext, useState } from "react";

export const colorSwitchContext = createContext({});

const Provider = colorSwitchContext.Provider;

export const ColorSwitchProvider = ({ children }) => {
    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const color = isRed ? 'red' : 'blue';

    const value = {
        color,
        handleSwitch
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}