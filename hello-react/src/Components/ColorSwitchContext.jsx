import { colorSwitchContext } from "Context/colorSwitchContext"
import { Btn, Rows } from "UIKit"
import { useContext, useState } from "react"

export const ColorSwitchContext = () => {
    //shared state
    const { color, handleSwitch } = useContext(colorSwitchContext);

    //render
    const styleCss = {
        color
    }
    return (
        <Rows>
            <h3 style={styleCss}>Color Switch</h3>
            <div>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </div>
        </Rows>
    )
}