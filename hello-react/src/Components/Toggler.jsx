import { Btn, Rows } from "UIKit"
import { useState } from "react"

export const Toggler = ({ children }) => {
    const [isDisp, setIsDisp] = useState(true);

    const handleToggle = () => {
        setIsDisp(!isDisp);
    }

    return (
        <Rows>
            <Btn onClick={handleToggle}>Toggle, {isDisp + ''}</Btn>

            <div>
                {isDisp && children}
            </div>
        </Rows>
    )
}