const Greeting = (props) => {
    console.log(props);

    const styleCss = {
        color: 'red',
        backgroundColor: 'yellow'
    }

    return (
        <div>
            <h2 style={styleCss}>Welcome {props.name} to our website</h2>
            {props.children}
        </div>
    )
}

export default Greeting;