import { useCounter } from "Hooks/useCounter";
import { Btn, Line, Rows } from "UIKit";
import { useNavigate } from "react-router-dom";

/*
    1) create a fully working component
    2) separate the logic and the render part
    3) create a function for the logic (starting with "useXX")
    4) move all of our logic to the new "use" function
    5) make sure the "use" function return all the variables needed for the parent component
    6) move the "use" hook to a different location and import it
*/

/*
    create a useColorSwitch hook 
    and change the color when the button clicks
*/

export const CounterHook = () => {
    const navigate = useNavigate();

    //logic
    const { count, handleAdd } = useCounter(5);

    const handleGoto = () => {
        navigate('/states');
    }

    const styleCss = {
        color: 'red'
    }
    //render
    return (
        <Rows>
            <h1 style={styleCss}>Count, {count}</h1>
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleGoto}>Goto States</Btn>
            </Line>
        </Rows>
    )
}