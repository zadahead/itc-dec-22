import { Btn, Line, Rows } from "UIKit"
import { useState } from "react"

export const ColorSwitch = () => {
    const [color, setColor] = useState('red');
    const [value, setValue] = useState('');

    console.log('value', value);

    const handleSwitch = () => {
        setColor(value);
    }

    const handleChange = (e) => {
        const value = e.target.value;
        setValue(value);
    }

    const styleCss = {
        backgroundColor: color,
        width: '100px',
        height: '100px'
    }

    return (
        <Rows>
            <h2>Color Switch</h2>
            <div style={styleCss}></div>
            <Line>
                <input value={value} onChange={handleChange} />
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Line>
        </Rows>
    )
}