import { Btn, Rows, Line } from "UIKit"
import { useState } from "react"

/*

    state
    count = 55

    render 
    <h2>Count, 55<h2>

    handle
    <btn>Add<btn>
*/

export const Counter = ({ count, setCount, prefix }) => {

    console.log('render', count);

    const handleAdd = () => {
        setCount(count + prefix);
    }

    const handleReduce = () => {
        setCount(count - 1);
    }

    const handleChange = (e) => {
        setCount(+e.target.value);
    }

    return (
        <Rows>
            <h3>Count, {count}</h3>
            <input value={count} onChange={handleChange} />
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleReduce}>Reduce</Btn>
            </Line>
        </Rows>
    )
}