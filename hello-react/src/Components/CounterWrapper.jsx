import { Rows } from "UIKit"
import { Counter } from "./Counter"
import { useState } from "react";

export const CounterWrapper = () => {
    const [count, setCount] = useState(10);

    const props = {
        count,
        setCount
    }

    return (
        <Rows>
            <Counter {...props} prefix={5} />
            <Counter {...props} prefix={1} />
        </Rows>
    )
}