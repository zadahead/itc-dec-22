import { useWindowWidth } from "Hooks/useWindowWidth";
import { Rows } from "UIKit";



export const WindowWidth = () => {
    //logic
    const width = useWindowWidth();

    //render
    return (
        <Rows>
            <h1>Current width: {width}</h1>
        </Rows>
    )
}