import { Btn, Rows } from "UIKit"
import { useState } from "react"

export const CounterBasic = () => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <Rows>
            <h3>Count, {count}</h3>
            <div>
                <Btn onClick={handleAdd}>Add</Btn>
            </div>
        </Rows>
    )
}