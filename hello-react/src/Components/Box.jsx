const Box = (props) => {

    const styleCss = {
        backgroundColor: '#e1e1e1',
        border: '1px solid #eee',
        padding: '20px'
    }

    return (
        <div>
            <h1>{props.title}</h1>
            <h2>{props.info}</h2>
            <div style={styleCss}>
                {props.children}
            </div>
        </div>
    )
}

export default Box;