import { useColorSwitch } from "Hooks/useColorSwitch";
import { Btn, Rows } from "UIKit"

/*
    1) create a fully working component
    2) separate the logic and the render part
    3) create a function for the logic (starting with "useXX")
    4) move all of our logic to the new "use" function
    5) make sure the "use" function return all the variables needed for the parent component
    6) call the "use" function from the parent component
    7) move the "use" hook to a different location and import it
*/


export const ColorSwitchHook = () => {
    //logic
    const { color, handleSwitch } = useColorSwitch('green', 'cyan');

    //render

    const styleCss = {
        color: 'yellow',
        backgroundColor: color
    }


    return (
        <Rows>
            <h2 style={styleCss}>Change Color</h2>
            <div>
                <Btn onClick={handleSwitch}>switch</Btn>
            </div>
        </Rows>
    )
}