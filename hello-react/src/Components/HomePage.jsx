import Greeting from './Greeting';

const HomePage = (props) => {
    return (
        <div>
            <h1>ITC - Dec - 2022</h1>
            <Greeting age={25} name={props.name}>
                <h2>Will this be shown?</h2>
            </Greeting>
        </div>
    )
}

export default HomePage;