import { countContext } from "Context/countContext"
import { Btn, Rows } from "UIKit"
import { useContext } from "react"

export const CountContext = () => {
    const { handleAdd, count } = useContext(countContext);

    return (
        <Rows>
            <h1>Count, {count}</h1>
            <div>
                <Btn onClick={handleAdd}>Add</Btn>
            </div>
        </Rows>
    )
}