import { Btn, Rows } from "UIKit"
import { useState } from "react"

export const Conditional = () => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const getInfo = () => {
        if (count > 12) {
            return <h4>this is bigger then 12</h4>;
        }

        if (count > 10) {
            return null;
        }

        if (count >= 5) {
            return <h4>Bigger or equal to 5</h4>
        }
        return <h4>Smaller then 5</h4>
    }

    return (
        <Rows>
            <h3>Conditional ({count})</h3>
            {getInfo()}
            <Btn onClick={handleAdd}>Add</Btn>
        </Rows>
    )
}