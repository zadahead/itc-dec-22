
import { NavLink, Route, Routes, useNavigate } from 'react-router-dom';
import './App.css';

import { Line, Between, Icon, Rows, Grid, Btn, BtnSecond, LinkBtn } from 'UIKit';
import { HomeView } from 'Views/HomeView';
import { AboutView } from 'Views/AboutView';
import { StatesView } from 'Views/StatesView';
import { ListView } from 'Views/ListView';
import { CycleView, TodosCycleView } from 'Views/CycleView';
import { RefView } from 'Views/RefView';
import { DDView } from 'Views/DDView';
import { HooksView } from 'Views/HooksViews';
import { useContext } from 'react';
import { TodosView } from 'Views/TodosView';
import { Callback } from 'Views/Callback';
import { AdminView } from 'Views/Admin';
import { RegisterView } from 'Views/RegisterView';
import { LoginView } from 'Views/LoginView';
import { authContext } from 'Auth/authContext';
import { AuthRoute } from 'Auth/AuthRoute';



const App = () => {
    const { isLogin, logout } = useContext(authContext);


    return (
        <div className="App">

            <Grid>
                <header>
                    <Between>
                        <Line>
                            <Icon i="menu" />
                            <NavLink to='/'>Home</NavLink>
                            <NavLink to='/admin'>Admin</NavLink>
                        </Line>
                        <Line>
                            {isLogin && <LinkBtn onClick={logout}>Logout</LinkBtn>}
                            {/* <NavLink to='/list'>List</NavLink>
                            <NavLink to='/cycle'>Cycle</NavLink>
                            <NavLink to='/ref'>Ref</NavLink>
                            <NavLink to='/dropdown'>Dropdown</NavLink>
                            <NavLink to='/hooks'>Hooks</NavLink>
                            <Line>
                                <NavLink to='/todos'>todos</NavLink>
                                <h3>{notCompleted}</h3>
                            </Line>
                            <NavLink to='/callback'>Callback</NavLink> */}
                        </Line>
                    </Between>
                </header>

                <section>
                    <Routes>
                        <Route path='/' element={<AuthRoute elem={HomeView} />} />
                        <Route path='/about' element={<AboutView />} />
                        <Route path='/states' element={<StatesView />} />
                        <Route path='/list' element={<ListView />} />
                        <Route path='/cycle' element={
                            <Rows>
                                <CycleView />
                                <TodosCycleView />
                            </Rows>
                        } />
                        <Route path='/ref' element={(
                            <Rows>
                                <RefView />
                                <RefView />
                            </Rows>
                        )} />
                        <Route path='/dropdown' element={<DDView />} />
                        <Route path='/hooks' element={<HooksView />} />
                        <Route path='/todos' element={<TodosView />} />
                        <Route path='/callback' element={<Callback />} />
                        <Route path='/admin/*' element={<AuthRoute elem={AdminView} />} />
                        <Route path='/register' element={<RegisterView />} />
                        <Route path='/login' element={<LoginView />} />
                    </Routes>
                </section>

                <footer>
                    <Line>
                        <h4>ITC Dec 2022</h4>
                    </Line>
                </footer>
            </Grid>
        </div>
    )
}

export default App;