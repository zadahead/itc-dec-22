import { ColorSwitchContext } from "Components/ColorSwitchContext"
import { ColorSwitchHook } from "Components/ColorSwitchHook"
import { CountContext } from "Components/CountContext"
import { CounterHook } from "Components/CounterHook"
import { WindowWidth } from "Components/WindowWidth"
import { calc } from "Helper/calc"
import { Rows } from "UIKit"

export const HooksView = () => {
    const result = calc(10, 15);

    return (
        <Rows>
            <h1>Hooks View, {result}</h1>
            {/* <CounterHook /> */}
            {/* <ColorSwitchHook /> */}
            {/* <WindowWidth /> */}
            <CountContext />
            <ColorSwitchContext />
        </Rows>
    )
}