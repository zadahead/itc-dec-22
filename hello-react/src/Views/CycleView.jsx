import { Fetch, Rows } from "UIKit";

export const CycleView = () => {
    //logic
    return (
        <Rows>
            <h1>Cycle</h1>
            {<Fetch url="/users" />}
        </Rows>
    )
}

export const TodosCycleView = () => {

    return (
        <Rows>
            <h1>Todos</h1>
            {<Fetch url="/todos" />}
        </Rows>
    )
}