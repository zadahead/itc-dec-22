import { todosContext } from "Context/todosContext";
import { Between, BetweenSpread, Box, Btn, Checkbox, Icon, Input, Line, Rows } from "UIKit"
import { useContext, useEffect, useRef, useState } from "react";



export const TodosView = () => {
    const context = useContext(todosContext);

    const {
        list,
        value,
        setValue,
        handleAdd,
        handleToggle,
        handleDelete
    } = context;

    const inputRef = useRef();

    useEffect(() => {
        inputRef.current.focus();
    }, [list])





    return (
        <Rows>
            <h1>Todos View</h1>

            <Box>
                <Rows>
                    <BetweenSpread>
                        <Input ref={inputRef} value={value} onChange={setValue} placeholder="Add new todo..." />
                        <Btn onClick={handleAdd}>Add</Btn>
                    </BetweenSpread>

                    {
                        list.map(todo => {
                            return (
                                <Between key={todo.id}>
                                    <Line>
                                        <Checkbox
                                            checked={todo.completed}
                                            onChange={() => handleToggle(todo)}
                                        >
                                            {todo.title}
                                        </Checkbox>
                                    </Line>
                                    <Icon i="delete" onClick={() => handleDelete(todo)} />
                                </Between>
                            )
                        })
                    }


                </Rows>
            </Box>
        </Rows>
    )
}