import { Dropdown, Rows } from "UIKit"
import { useState } from "react"

const list = [
    {
        id: 1,
        value: 'Mosh'
    },
    {
        id: 2,
        value: 'David'
    },
    {
        id: 3,
        value: 'Ruth'
    }
]

export const DDView = () => {
    const [selected, setSelected] = useState(null);

    return (
        <Rows>
            <h1>Dropdown View</h1>
            <Dropdown
                list={list}
                selected={selected}
                onChange={setSelected}
            />
            <h4>assad</h4>
        </Rows>
    )
}