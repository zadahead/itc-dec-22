import { Rows, Center, Box, Input, Btn, Msg } from "UIKit"
import axios from "axios";
import { useState } from "react"
import { Link, useNavigate } from "react-router-dom";

export const RegisterView = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [error, setError] = useState('');

    const navigate = useNavigate();

    const handleRegister = () => {
        if (!email || !password || !name) {
            return;
        }

        axios.post(process.env.REACT_APP_API_PORT + 'users', {
            email,
            password,
            name
        }).then(resp => {
            navigate('/login');
        }).catch(err => {
            console.log(err);
            //setError(err.response.data);
        })
    }

    return (
        <div>
            <Center>
                <Box>
                    <Rows>
                        <Center>
                            <h1>Register</h1>
                        </Center>
                        <div>
                            <Input value={name} onChange={setName} placeholder="your name" />
                        </div>
                        <div>
                            <Input value={email} onChange={setEmail} placeholder="your email" />
                        </div>
                        <div>
                            <Input value={password} onChange={setPassword} placeholder="your password" />
                        </div>
                        <Center>
                            <Btn onClick={handleRegister}>Register</Btn>
                        </Center>
                        <Center>
                            <Link to='/login'>Already registered? login</Link>
                        </Center>
                        <Center>
                            {error && <Msg msg={error} isError={true} />}
                        </Center>
                    </Rows>
                </Box>
            </Center>
        </div>
    )
}