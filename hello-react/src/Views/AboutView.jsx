import { Rows, Line, Btn } from "UIKit"

export const AboutView = () => {
    const handleClick = () => {
        console.log('CLICKED!');
    }

    return (
        <Rows>
            <h1>About Us</h1>
            <Line>
                <Btn onClick={handleClick}>Click me</Btn>

                <Btn i="home" onClick={handleClick} >Click with icon</Btn>

            </Line>
        </Rows>
    )
}