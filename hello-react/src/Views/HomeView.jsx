import { api } from "Api/api"
import { Between, Btn, Dropdown, Input, Line, ListItem, Rows } from "UIKit"
import { useEffect, useState } from "react"

const sortOptions = [
    { id: 'asc-name', value: 'Name A-Z' },
    { id: 'desc-name', value: 'Name Z-A' },
    { id: 'asc-price', value: 'Price A-Z' },
    { id: 'desc-price', value: 'Price Z-A' }
]

export const HomeView = () => {
    const [sort, setSort] = useState(null);
    const [search, setSearch] = useState('');
    const [list, setList] = useState([]);

    const [skip, setSkip] = useState(1);
    const [limit, setLimit] = useState(10);

    useEffect(() => {
        loadItems();
    }, [sort])

    const addNewOrder = () => {
        api.send('post', '/orders/64b804b6d5732840dddcadfb', {
            items: ['64a7b1629324d29e096d932c', '64b107157c987fc0db5eb177']
        }).then(resp => {
            console.log(resp);
        })
    }

    const loadItems = () => {
        const params = {};
        if (search) { params.q = search }
        if (sort) { params.sort = sort }
        if (skip) { params.skip = skip }
        if (limit) { params.limit = limit }

        api.send('get', '/foods', params).then(resp => {
            setList(resp);
        })
    }

    const handleSearch = () => {
        loadItems();
    }

    const handleSort = (value) => {
        setSort(value);
    }

    console.log(sort)

    return (
        <Rows>
            <Line>
                <h1>Our Foods Collection</h1>
                <Btn onClick={addNewOrder}>Place order</Btn>
            </Line>
            <Between>
                <Line>
                    <Input placeholder="limit" value={limit} onChange={setLimit} />
                    <Input placeholder="skip" value={skip} onChange={setSkip} />
                    <Input placeholder="search food.." value={search} onChange={setSearch} />
                    <Btn onClick={handleSearch}>Search</Btn>
                    <Dropdown list={sortOptions} selected={sort} onChange={handleSort} />
                </Line>
            </Between>

            <Line>
                {list.map(item => (
                    <ListItem key={item._id}>
                        <h3>{item.name}</h3>
                        <div className="pic">
                            <img src={item.img} alt="pic" />
                        </div>
                        <h4>{item.info}</h4>
                        <h5>Price: {item.price}</h5>
                    </ListItem>
                ))}
            </Line>
        </Rows>
    )
}