import { ColorSwitch } from "Components/ColorSwitch"
import { Conditional } from "Components/Conditional";
import { Counter } from "Components/Counter"
import { CounterBasic } from "Components/CounterBasic";
import { CounterWrapper } from "Components/CounterWrapper";
import { Toggler } from "Components/Toggler";
import { Btn, Checkbox, Rows, Checkboxes, Radiobtns } from "UIKit";
import { Radiobtn } from "UIKit/Elements/Radiobtn/Radiobtn";
import { useState } from "react";

const list = [
    {
        id: 'hide_network',
        title: 'Hide network',
        checked: true
    },
    {
        id: 'preserve_log',
        title: 'Preserve log',
        checked: false
    }
]

const list2 = [
    {
        id: 1,
        title: 'Mosh'
    },
    {
        id: 2,
        title: 'David'
    },
    {
        id: 3,
        title: 'Baruch'
    },
    {
        id: 4,
        title: 'Ruth'
    }
]

export const StatesView = () => {
    const [settings, setSettings] = useState(list);
    const [user, setUser] = useState(null);

    const handleSubmit = () => {
        console.log('settings:', settings);
        console.log('selected user:', user);
    }

    return (
        <Rows>
            <h1>States</h1>

            <h4>Settings:</h4>
            <Checkboxes list={settings} onChange={setSettings} />

            <h4>Pick User:</h4>
            <Radiobtns list={list2} selected={user} onChange={setUser} />

            <div>
                <Btn onClick={handleSubmit}>Show Selected</Btn>
            </div>
        </Rows>
    )
}