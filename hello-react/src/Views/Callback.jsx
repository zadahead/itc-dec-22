import { Btn, Rows } from "UIKit"
import { memo, useCallback, useState } from "react";

export const Callback = () => {
    const [count, setCount] = useState(0);

    const name = count > 3 ? 'mosh' : 'david';

    const handleAdd = () => {
        setCount(count + 1)
    }

    const handleSomething = useCallback(() => {
        console.log('handleSomething', name)
    }, [name])

    console.log('Callback-Component');
    return (
        <Rows>
            <h3>Callback, {count}</h3>
            <Btn onClick={handleAdd}>Add</Btn>
            <InnerComponent name={name} setCount={handleSomething} />
        </Rows>
    )
}


const InnerComponent = memo(({ name, setCount }) => {
    console.log('Inner-Component')
    return (
        <div>
            InnerComponent, {name}
            <DeeperComponent />
        </div>
    )
})

const DeeperComponent = () => {
    console.log('Deeper-Component')

    return (
        <div>
            DeeperComponent
        </div>
    )
}