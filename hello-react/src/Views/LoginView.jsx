import { authContext } from "Auth/authContext";
import { Box, Btn, Center, Input, Msg, Rows } from "UIKit"
import axios from "axios";
import { useContext, useState } from "react"
import { Link } from "react-router-dom";

export const LoginView = () => {
    const { login } = useContext(authContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');

    const handleLogin = () => {
        if (!email || !password) {
            return;
        }

        axios.post(process.env.REACT_APP_API_PORT + 'auth/login', {
            email,
            password
        }).then(resp => {
            login(resp.data.access_token);
        }).catch(err => {
            setError(err.response.data)
        })
    }

    return (
        <div>
            <Center>
                <Box>
                    <Rows>
                        <Center>
                            <h1>Login</h1>
                        </Center>
                        <div>
                            <Input value={email} onChange={setEmail} placeholder="your email" />
                        </div>
                        <div>
                            <Input value={password} onChange={setPassword} placeholder="your password" />
                        </div>
                        <Center>
                            <Btn onClick={handleLogin}>Login</Btn>
                        </Center>
                        <Center>
                            <Link to='/register'>Not a user? register</Link>
                        </Center>
                        <Center>
                            {error && <Msg msg={error} isError={true} />}
                        </Center>
                    </Rows>
                </Box>
            </Center>
        </div>
    )
}