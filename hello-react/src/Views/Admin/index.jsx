import { Line, Rows } from "UIKit"
import { Link, NavLink, Route, Routes } from "react-router-dom"
import { UsersView } from "./Users"
import { FoodsView } from "./Foods"

export const AdminView = () => {

    return (
        <Rows>
            <Line>
                <h1>Admin View</h1>
                <NavLink to='users' >Users</NavLink>
                <NavLink to='foods' >Foods</NavLink>
            </Line>


            <Routes>
                <Route path='/users' element={<UsersView />} />
                <Route path='/foods' element={<FoodsView />} />
            </Routes>
        </Rows>
    )
}