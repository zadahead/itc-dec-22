
import { api } from "Api/api";

export const usersApi = {
    get: async () => {
        return api.send('get', '/users');
    },

    add: async (payload) => {
        return api.send('post', '/users', payload);
    },

    update: async (itemId, payload) => {
        return api.send('put', '/users/' + itemId, payload);
    },

    remove: async (itemId) => {
        return api.send('delete', '/users/' + itemId);
    }
}
