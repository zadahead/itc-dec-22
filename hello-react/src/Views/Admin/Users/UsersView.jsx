import { Btn, Input, Line, Msg, Rows, StockItem } from "UIKit";
import { useEffect, useState } from "react"
import { usersApi } from "./helpers/usersApi";

//CRUD 

/*
    V collect 
    V api 
    callback
    catch
*/

export const UsersView = () => {
    const [msg, setMsg] = useState(null);
    const [editItem, setEditItem] = useState(null);
    const [items, setItems] = useState([]);

    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');


    useEffect(() => {
        loadItems();
    }, [])

    const loadItems = () => {
        usersApi.get()
            .then(resp => {
                setItems(resp);
            })
            .catch(handleError)
    }

    const handleSave = () => {
        if (editItem) {
            handleEdit();
        } else {
            handleAdd();
        }
    }

    const showError = (msg) => {
        setMsg({ msg, isError: true });
    }

    const showSuccess = (msg) => {
        setMsg({ msg });
    }

    const getItem = () => {
        if (!name || !email || !password) {
            showError("empty values are not allowed");
            return;
        }

        return {
            name,
            email,
            password
        }
    }

    const handleCallback = (msg) => {
        clearInputs();
        loadItems();
        showSuccess(msg);
    }

    const handleError = (err) => {
        showError(err.response.data);
    }

    const handleEdit = () => {

        const item = getItem();
        if (!item) { return }

        usersApi.update(editItem, item)
            .then(() => handleCallback("update success"))
            .catch(handleError)
    }

    const handleAdd = () => {

        const item = getItem();
        if (!item) { return }

        usersApi.add(item)
            .then(() => handleCallback("item added successfully"))
            .catch(handleError)
    }

    const handeDelete = (itemId) => {
        usersApi.remove(itemId)
            .then(() => handleCallback("item removed successfully"))
            .catch(handleError)
    }

    const handleEditMode = (item) => {
        setName(item.name);
        setEmail(item.email);
        setPassword(item.password);
        setEditItem(item._id);
    }


    const clearInputs = () => {
        setName('');
        setEmail('');
        setPassword('');
        setEditItem(null)
    }


    return (
        <Rows>
            <Line>
                <h1>Admin View</h1>
                <Msg {...msg} />
            </Line>
            <div>
                <Line>
                    <Input placeholder="name" value={name} onChange={setName} />
                    <Input placeholder="email" value={email} onChange={setEmail} />
                    <Input placeholder="password" value={password} onChange={setPassword} />
                    <Btn onClick={handleSave}>{editItem ? 'Update' : 'Add'}</Btn>
                    {
                        editItem && <h3 onClick={clearInputs}>Cancel</h3>
                    }
                </Line>
            </div>
            <Rows>
                {
                    items.map(item => {
                        return (
                            <Line key={item._id}>
                                <StockItem
                                    onDelete={() => handeDelete(item._id)}
                                    onEdit={() => handleEditMode(item)}
                                >
                                    <Rows>
                                        <h3>{item.name}</h3>
                                        <h4>{item.email}</h4>
                                    </Rows>
                                </StockItem>
                            </Line>
                        )
                    })
                }
            </Rows>
        </Rows>
    )
}