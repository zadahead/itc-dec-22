import { Btn, Input, Line, Msg, Rows, StockItem } from "UIKit";
import { useEffect, useState } from "react"
import { foodsApi } from "./helpers/foodApi";

//CRUD 

/*
    V collect 
    V api 
    callback
    catch
*/

export const FoodsView = () => {
    const [msg, setMsg] = useState(null);
    const [editItem, setEditItem] = useState(null);
    const [items, setItems] = useState([]);

    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [img, setImg] = useState('');
    const [info, setInfo] = useState('');


    useEffect(() => {
        loadItems();
    }, [])

    const loadItems = () => {
        foodsApi.get()
            .then(resp => {
                setItems(resp);
            })
            .catch(handleError)
    }

    const handleSave = () => {
        if (editItem) {
            handleEdit();
        } else {
            handleAdd();
        }
    }

    const showError = (msg) => {
        setMsg({ msg, isError: true });
    }

    const showSuccess = (msg) => {
        setMsg({ msg });
    }

    const getItem = () => {
        if (!name || !img || !price || !info) {
            showError("empty values are not allowed");
            return;
        }

        return {
            name,
            info,
            img,
            price: +price
        }
    }

    const handleCallback = (msg) => {
        clearInputs();
        loadItems();
        showSuccess(msg);
    }

    const handleError = (err) => {
        showError(err.response.data);
    }

    const handleEdit = () => {

        const item = getItem();
        if (!item) { return }

        foodsApi.update(editItem, item)
            .then(() => handleCallback("update success"))
            .catch(handleError)
    }

    const handleAdd = () => {

        const item = getItem();
        if (!item) { return }

        foodsApi.add(item)
            .then(() => handleCallback("item added successfully"))
            .catch(handleError)
    }

    const handeDelete = (itemId) => {
        foodsApi.remove(itemId)
            .then(() => handleCallback("item removed successfully"))
            .catch(handleError)
    }

    const handleEditMode = (item) => {
        console.log(item);
        setName(item.name);
        setInfo(item.info);
        setPrice(item.price);
        setImg(item.img);
        setEditItem(item._id);
    }


    const clearInputs = () => {
        setName('');
        setInfo('')
        setPrice('');
        setImg('');
        setEditItem(null)
    }


    return (
        <Rows>
            <Line>
                <h1>Admin View</h1>
                <Msg {...msg} />
            </Line>
            <div>
                <Line>
                    <Input placeholder="name" value={name} onChange={setName} />
                    <Input placeholder="info" value={info} onChange={setInfo} />
                    <Input placeholder="price" value={price} onChange={setPrice} />
                    <Input placeholder="img" value={img} onChange={setImg} />
                    <Btn onClick={handleSave}>{editItem ? 'Update' : 'Add'}</Btn>
                    {
                        editItem && <h3 onClick={clearInputs}>Cancel</h3>
                    }
                </Line>
            </div>
            <Rows>
                {
                    items.map(item => {
                        return (
                            <Line key={item._id}>
                                <StockItem
                                    onDelete={() => handeDelete(item._id)}
                                    onEdit={() => handleEditMode(item)}
                                >
                                    <Rows>
                                        <h3>{item.name}</h3>
                                        <h4>{item.info}</h4>
                                        <div className="pic">
                                            <img src={item.img} alt="pic" />
                                        </div>
                                        <h5>Price: {item.price}</h5>
                                    </Rows>
                                </StockItem>
                            </Line>
                        )
                    })
                }
            </Rows>
        </Rows>
    )
}