
import { api } from "Api/api";

export const foodsApi = {

    get: async () => {
        return api.send('get', '/foods')
    },

    add: async (payload) => {
        return api.send('post', '/foods', payload);
    },

    update: async (itemId, payload) => {
        return api.send('put', '/foods/' + itemId, payload);
    },

    remove: async (itemId) => {
        return api.send('delete', '/foods/' + itemId);
    }
}
