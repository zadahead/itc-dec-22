import { Btn, Line, Rows } from "UIKit"
import { useState } from "react"

const users = [
    { id: 1, name: 'Mosh111' },
    { id: 2, name: 'David' },
    { id: 3, name: 'Ruth' }
]
export const ListView = () => {
    const [list, setList] = useState(users);
    const [inputValue, setInputValue] = useState('');

    const handleAdd = () => {
        const newList = [
            ...list,
            {
                id: crypto.randomUUID(),
                name: inputValue
            }
        ]

        setList(newList);
    }

    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    }

    const renderList = () => {
        return list.map((i) => {
            return <h3 key={i.id}>{i.name}</h3>
        })
    }

    return (
        <Rows>
            <h1>List View</h1>
            <Line>
                <input value={inputValue} onChange={handleInputChange} />
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>

            {renderList()}
        </Rows>
    )
}