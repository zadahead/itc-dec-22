import { Btn, Rows } from "UIKit";
import { useEffect, useRef, useState } from "react"

// const countRef = {
//     current: 0
// }

export const RefView = () => {
    const [count, setCount] = useState(0);
    const h1Ref = useRef();

    const countRef = useRef();
    countRef.current = count;



    useEffect(() => {
        setTimeout(() => {
            console.log(h1Ref);

            if (countRef.current > 3) {
                h1Ref.current.style.backgroundColor = 'red';
            }

        }, 3000)
    }, [])

    const handleAdd = () => {
        setCount(count + 1)
    }


    return (
        <Rows>
            <h1 ref={h1Ref}>Ref View</h1>
            <h3>Count, {count}</h3>
            <div>
                <Btn onClick={handleAdd}>Add</Btn>
            </div>
        </Rows>
    )
}