import { useEffect, useState } from "react";

export const useWindowWidth = () => {
    const [width, setWidth] = useState(getWidth());

    useEffect(() => {
        const handleSetWidth = () => {
            setWidth(getWidth())
        }

        window.addEventListener('resize', handleSetWidth);

        return () => {
            window.removeEventListener('resize', handleSetWidth);
        }
    }, [])


    function getWidth() {
        return window.innerWidth;
    }

    return width;
}