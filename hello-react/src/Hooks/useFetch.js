import axios from "axios";
import { useEffect, useState } from "react";

export const useFetch = (url) => {
    const [error, setError] = useState('');
    const [isLoading, setIsLoading] = useState(true);
    const [list, setList] = useState([]);
    const DOMAIN = 'https://jsonplaceholder.typicode.com';

    useEffect(() => {
        axios.get(DOMAIN + url)
            .then(resp => {
                setTimeout(() => {
                    setList(resp.data);
                    setIsLoading(false);
                }, 2000)
            })
            .catch(err => {
                setError(err.message)
            })

        return () => {
            console.log('will unmount')
        }
    }, [])

    return {
        isLoading,
        list,
        error
    }
}
