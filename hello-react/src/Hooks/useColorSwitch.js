import { useState } from "react";

export const useColorSwitch = (colorA = 'red', colorB = 'blue') => {
    const [color, setColor] = useState(colorA);

    const handleSwitch = () => {
        setColor(color === colorA ? colorB : colorA);
    }

    return {
        color,
        handleSwitch
    }
}