import { useState } from "react";

export const useCounter = (initialValue = 0) => {
    const [count, setCount] = useState(initialValue);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return {
        count,
        handleAdd
    }
}