const express = require('express');
const app = express();

//GET

app.get('/pizza', (req, res) => {
    res.send('enjoy the pizza');
})

app.get('/pasta', (req, res) => {
    res.send([1, 2, 3]);
})

app.get('*', (req, res) => {
    res.send('What would you like to order')
})

app.listen(4000, () => {
    console.log('Express server is running on port 4000');
})