const http = require('http');

const server = http.createServer((req, res) => {
    console.log(req.url);


    res.setHeader('Access-Control-Allow-Origin', '*');
    //res.setHeader('Content-Type', 'text/javascript');

    switch (req.url) {
        case '/pasta':
            res.write('enjoy the pasta');
            break;
        case '/pizza':
            res.write('enjoy the pizza');
            break;
        default:
            res.write('what would you like to order?');
            break;
    }


    res.end();
});

server.listen(3000, () => {
    console.log('server is listening on port 3000');
})