const express = require('express');
const app = express();

/*
    1) Customer 1 ordered 2 Pizzas
    2) Customer 2 ordered 1 Pizza and 1 Pasta
*/

/*
     {
        "id": 1,
        "customer": {
            id: 1,
            name: "Mosh"
        },
        "foods": [
            {
                food: {
                    id: 1,
                    name: "Pizza"
                },
                count: 2
            }
        ]
    }
*/

const orders = [
    {
        id: 1,
        customerId: 1,
        foods: [
            {
                foodId: 1,
                count: 2
            }
        ]
    },

    {
        id: 2,
        customerId: 2,
        foods: [
            {
                foodId: 1,
                count: 1
            },
            {
                foodId: 2,
                count: 1
            }
        ]
    }
]

const customers = [
    {
        id: 1,
        name: 'Mosh'
    },
    {
        id: 2,
        name: 'David22'
    }
]

const foods = [
    {
        id: 1,
        name: "Pizza"
    },
    {
        id: 2,
        name: "Pasta"
    }
]

app.get('/customers', (req, res) => {
    res.send(customers);
})

app.get('/customers/:id', (req, res) => {
    const item = customers.find((c) => c.id === +req.params.id);
    res.send(item);
})

app.get('/foods', (req, res) => {
    res.send(foods);
})

app.get('/foods/:id', (req, res) => {
    const item = foods.find((f) => f.id === +req.params.id);
    res.send(item);
})

app.get('/orders', (req, res) => {
    res.send(orders);
})

/*
    order 
    {
        id: 1,
        customerId: 1,
        foods: [
            {
                foodId: 1,
                count: 2
            }
        ]
    }
*/

app.get('/orders/:id', (req, res) => {
    const order = orders.find(order => order.id === +req.params.id);

    const rtn = {
        id: order.id,
        customer: customers.find(c => c.id === order.customerId),
        foods: order.foods.map(food => {
            return {
                food: foods.find(f => f.id === food.foodId),
                count: food.count
            }
        })
    }
    res.send(rtn);
})

app.listen(5999, () => {
    console.log("Rest API is listening on PORT 5999")
})