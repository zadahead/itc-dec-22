const calc = (a, b) => {
    return a + b;
}

const mult = (a, b) => {
    return a * b;
}

module.exports = {
    calc,
    mult
};