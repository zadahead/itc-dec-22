const express = require('express');
const path = require('path');
const app = express();
const fs = require('fs');


app.get('*', (req, res) => {
    console.log(req.url);

    const content = fs.readFileSync(path.join(__dirname, 'index.html'), 'utf-8');
    console.log(content);


    //res.sendFile(path.join(__dirname, 'index.html'));
    res.send(content.replace('{{content}}', req.url));
})

app.listen(5000, () => {
    console.log("Webserver is running on port 5000");
})