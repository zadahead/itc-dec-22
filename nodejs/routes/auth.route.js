const express = require('express');
const route = express.Router();
const DB = require('../db');
const { validateSchema } = require('../schema/validate');
const { authLoginSchema } = require('../schema/auth.login.schema');
const bcrypt = require('bcrypt');
const { sign } = require('../utils/jwt');
const { findUserByEmailService } = require('../services/users.service');

route.post('/login', validateSchema(authLoginSchema), async (req, res) => {
    const { email, password } = req.body;

    const user = await findUserByEmailService(email);

    if (!user) {
        return res.status(400).send('user not exist or non valid password');
    }

    bcrypt.compare(password, user.password, function (err, valid) {
        // result == true
        if (valid) {

            const data = { id: user.id };
            const token = sign(data);

            res.send({ access_token: token });
        } else {
            return res.status(400).send('user not exist or non valid password');
        }
    });


})

module.exports = route;