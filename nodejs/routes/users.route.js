const express = require('express');
const { usersSchema } = require('../schema/users.schema');
const { validateSchema } = require('../schema/validate');
const route = express.Router();

const { addNewUser, getAllUsers, getUsersById, updateUser, deleteUser } = require('../controllers/user.controller');


route.get('/', getAllUsers);
route.get('/:id', getUsersById);
route.post('/', validateSchema(usersSchema), addNewUser);
route.put('/:id', validateSchema(usersSchema), updateUser);
route.delete('/:id', deleteUser);

module.exports = route;