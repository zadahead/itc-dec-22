const express = require('express');

const { validateSchema } = require('../schema/validate');
const { foodsSchema } = require('../schema/foods.schema');
const {
    getAllFoodsController,
    getFoodController,
    createFoodController,
    updateFoodController,
    delteFoodController
} = require('../controllers/foods.controller');

const route = express.Router();


route.get('/', getAllFoodsController);
route.get('/:id', getFoodController);
route.post('/', validateSchema(foodsSchema), createFoodController);
route.put('/:id', validateSchema(foodsSchema), updateFoodController);
route.delete('/:id', delteFoodController);

module.exports = route;