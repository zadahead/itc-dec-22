const express = require('express');
const route = express.Router();

const { getOrdersByOwner, addNewOrder } = require('../controllers/orders.controller');


route.get('/:id', getOrdersByOwner);
route.post('/:id', addNewOrder);

module.exports = route;