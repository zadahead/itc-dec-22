require('dotenv').config();

require('./utils/mongo');

const express = require('express');
const app = express();
const cors = require('cors');
const DB = require('./db');

const { verify } = require('./utils/jwt');



const users = new DB('users');

app.use(cors({
    origin: '*'
}))

app.use(express.json());

app.use((req, res, next) => {
    res.error = (err) => {
        console.log(err);
        const [status, msg] = err;
        res.status(status).send(msg)
    }

    next();
})

app.use((req, res, next) => {

    if (
        (req.method === 'POST' && req.url === '/auth/login') ||
        (req.method === 'POST' && req.url === '/users') ||
        (req.method === 'POST' && req.url.startsWith('/orders'))
    ) {
        return next();
    }

    const token = req.headers.userid;
    const data = verify(token);

    if (!data) {
        return res.status(401).send('user not allowed')
    }

    const me = users.getById(data.id);
    req.me = me;

    next();
})

app.use('/users', require('./routes/users.route'));
app.use('/foods', require('./routes/foods.route'));
app.use('/auth', require('./routes/auth.route'));
app.use('/orders', require('./routes/orders.route'));

app.use((error, req, res, next) => {
    res.status(500).send(error);
})

app.listen(4000, () => {
    console.log('Express is running on port 4000');
})
