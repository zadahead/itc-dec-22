const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

class DB {
    constructor(name) {
        this.name = name;
        this.path = path.join(__dirname, `db/${this.name}.json`);
    }

    get = () => {
        const content = fs.readFileSync(this.path, 'utf-8');
        const parsed = JSON.parse(content);
        return parsed;
    }

    save = (parsed) => {
        const stringified = JSON.stringify(parsed, null, 2);
        fs.writeFileSync(this.path, stringified, 'utf-8');
    }

    getById = (id) => {
        const parsed = this.get();

        const item = parsed.find(u => u.id === id);

        return item;
    }

    updateById = (id, body) => {
        const parsed = this.get();

        const itemIndex = parsed.findIndex(u => u.id === id);
        parsed[itemIndex] = { id, ...body }

        this.save(parsed);
    }

    addNew = (item) => {
        const parsed = this.get();

        parsed.push({
            ...item,
            id: crypto.randomUUID()
        })

        this.save(parsed);
    }

    removeById = (id) => {
        const parsed = this.get();
        const index = parsed.findIndex(u => u.id === id);
        parsed.splice(index, 1);

        this.save(parsed);
    }
}


module.exports = DB;
