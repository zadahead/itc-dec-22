const schema = {
    type: "object",
    properties: {
        name: { type: 'string' },
        img: { type: 'string' },
        info: { type: 'string' },
        price: { type: 'integer', minimum: 15, maximum: 100 }
    },
    required: ["name", "info", "price", "img"],
    additionalProperties: false
}

module.exports = {
    foodsSchema: schema
}