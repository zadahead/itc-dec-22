const { ObjectId } = require('mongodb');
const db = require('../utils/mongo');




const getOrderByOwnerService = async (userId) => {
    const user = await db.orders.findOne({ owner: new ObjectId(userId) })
    return user;
}

const addNewOrderService = async (order) => {
    const resp = await db.orders.insertOne(order);
    return resp;
}


module.exports = {
    getOrderByOwnerService,
    addNewOrderService
}