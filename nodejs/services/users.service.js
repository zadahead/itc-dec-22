const { ObjectId } = require('mongodb');
const db = require('../utils/mongo');



const getAllUsersService = async () => {
    const list = await db.users.find().toArray();
    return list;
}

const getUsersByIdService = async (userId) => {
    const user = await db.users.findOne({ _id: new ObjectId(userId) })
    return user;
}

const findUserByEmailService = async (email) => {
    const user = await db.users.findOne({ email: email })
    return user;
}

const findIsUserByEmailService = async (email, userId) => {
    const user = await db.users.findOne(
        {
            email: email,
            _id: {
                $ne: new ObjectId(userId)
            }
        }
    )
    return user;
}

const addNewUserService = async (user, hash) => {
    user.password = hash;
    console.log(user);

    const resp = await db.users.insertOne(user);
    return resp;
}

const updateUserService = async (userId, user) => {
    delete user.password;

    const resp = await db.users.updateOne(
        { _id: new ObjectId(userId) },
        {
            $set: user
        }
    )
    return resp;
}

const deleteUserService = async (userId) => {
    const resp = await db.users.deleteOne({ _id: new ObjectId(userId) });
    return resp;
}

module.exports = {
    getAllUsersService,
    getUsersByIdService,
    findUserByEmailService,
    addNewUserService,
    findIsUserByEmailService,
    updateUserService,
    deleteUserService
}