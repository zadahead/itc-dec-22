const { ObjectId } = require('mongodb');
const db = require('../utils/mongo');


const getAllFoodsService = async (query) => {
    const fields = {};
    const sort = {};
    const skip = (query.skip - 1) * query.limit;
    let limit = query.limit || 1;

    if (query.q) {
        const reg = { $regex: new RegExp(query.q, 'i') };

        fields.$or = [
            { name: reg },
            { info: reg }
        ]
    }
    if (query.sort) {
        const [dir, field] = query.sort.split('-');
        sort[field] = dir === 'asc' ? 1 : -1;
    }



    const resp = await db.foods.find(fields).sort(sort).limit(+limit).skip(+skip).toArray();
    return resp;
    //use mongo util for find all foods     
}

const getFoodByIdService = async (foodId) => {
    const resp = await db.foods.findOne({ _id: new ObjectId(foodId) })
    return resp;
}

const getFoodsByIdsService = async (foodIds) => {
    const map = foodIds.map(id => {
        return new ObjectId(id);
    })
    const resp = await db.foods.find({ _id: { $in: map } }).toArray();
    return resp;
}

const getFoodByNameService = async (name) => {
    const resp = await db.foods.findOne({ name });
    return resp;
}



const addNewFoodItemService = async (item) => {
    const resp = await db.foods.insertOne(item);
    return resp;
}

const updateFoodService = async (id, item) => {
    const resp = await db.foods.updateOne(
        { _id: new ObjectId(id) },
        {
            $set: item
        }
    )
    return resp;
}

const deleteFoodService = async (id) => {
    const resp = await db.foods.deleteOne({ _id: new ObjectId(id) });
    return resp;
}

module.exports = {
    getAllFoodsService,
    getFoodByIdService,
    addNewFoodItemService,
    getFoodByNameService,
    getFoodsByIdsService,
    updateFoodService,
    deleteFoodService
}