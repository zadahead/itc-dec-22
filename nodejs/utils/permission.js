const permissions = [
    { id: 0, role: 'user' },
    { id: 1, role: 'admin' },
    { id: 2, role: 'finance' },
    { id: 4, role: 'editor' },
    { id: 8, role: 'held_desk' }
]


const getPermissions = (permissionId) => {
    const map = {};

    permissions.forEach(pr => {
        if ((permissionId & pr.id) === pr.id) {
            map[pr.role] = true;
        }
    })

    return map;
}

module.exports = {
    getPermissions
}