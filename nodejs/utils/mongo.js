const { MongoClient, ServerApiVersion } = require('mongodb');

const uri = `mongodb+srv://${process.env.MONGO_URI}/?retryWrites=true&w=majority`;

const client = new MongoClient(uri, {
    serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: true
    }
})

const cols = {}

async function run() {
    try {
        await client.connect();
        const restaraunt = await client.db('restaraunt');

        const foods = restaraunt.collection('foods');
        const users = restaraunt.collection('users');
        const orders = restaraunt.collection('orders');

        //we have a collection 

        cols.foods = foods;
        cols.users = users;
        cols.orders = orders;

        // const resp = await foods.find().toArray();

        console.log("Mongo is connected")

        //read mongodb database -> collection - show list of documents
    } catch (error) {
        console.log(error)
    } finally {
        //await client.close();
    }
}

run();

module.exports = cols