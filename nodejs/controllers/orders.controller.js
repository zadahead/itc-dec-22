



const { getFoodsByIdsService } = require('../services/foods.service');
const { getOrderByOwnerService, addNewOrderService } = require('../services/orders.service');



const getOrdersByOwner = async (req, res) => {
    const userId = req.params.id;
    const user = await getOrderByOwnerService(userId);
    res.status(200).send(user);
}

const addNewOrder = async (req, res) => {
    console.log(req.params);
    const userId = req.params.id;

    const { items } = req.body;
    const foodItems = await getFoodsByIdsService(items);

    let total = 0;
    foodItems.forEach(item => {
        total += item.price;
    });


    const order = {
        owner: userId,
        products: foodItems,
        total
    }

    const resp = await addNewOrderService(order);

    res.status(201).send(resp);
    //userId
    //foods list by ids

    //sum up the total price

    //save that 

}


module.exports = {
    getOrdersByOwner,
    addNewOrder
}