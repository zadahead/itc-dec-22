



const { generatePasswordService } = require('../services/hash.service');
const { getAllUsersService, getUsersByIdService, findUserByEmailService, findIsUserByEmailService, updateUserService, deleteUserService, addNewUserService } = require('../services/users.service');


const getAllUsers = async (req, res) => {
    const users = await getAllUsersService();
    res.status(200).send(users);
}

const getUsersById = async (req, res) => {
    const userId = req.params.id;
    const user = await getUsersByIdService(userId);
    res.status(200).send(user);
}

const addNewUser = async (req, res) => {
    console.log("addNewUser")
    const { email, password } = req.body;

    const user = await findUserByEmailService(email);
    if (user) {
        return res.status(400).send("Email already exist")
    }


    try {
        generatePasswordService(password, async (hash) => {
            const resp = await addNewUserService(req.body, hash);

            res.status(201).send(resp);
        })

    } catch (error) {
        console.log(error);
        return res.status(500).send("Something went wrong")
    }
}

const updateUser = async (req, res) => {
    const userId = req.params.id;

    const { email } = req.body;

    const user = await findIsUserByEmailService(email, userId);
    if (user) {
        return res.status(400).send("Email already exist");
    }

    await updateUserService(userId, req.body);

    res.send('success');

}

const deleteUser = async (req, res) => {
    const userId = req.params.id;
    await deleteUserService(userId);
    res.send('success');
}

module.exports = {
    getAllUsers,
    getUsersById,
    addNewUser,
    updateUser,
    deleteUser
}