const {
    getAllFoodsService,
    getFoodByIdService,
    getFoodByNameService,
    addNewFoodItemService,
    updateFoodService,
    deleteFoodService
} = require("../services/foods.service");
const { alreadyExistError, unauthorisedError } = require("../utils/error");
const { getPermissions } = require("../utils/permission");

const getAllFoodsController = async (req, res) => {
    const list = await getAllFoodsService(req.query);
    res.status(200).send(list);
}

const getFoodController = async (req, res) => {
    const foodId = req.params.id;
    const food = await getFoodByIdService(foodId);
    res.status(200).send(food);
}

const createFoodController = async (req, res) => {
    const { name } = req.body;


    const existedFood = await getFoodByNameService(name);
    if (existedFood) {
        return res.error(alreadyExistError);
    }

    const resp = await addNewFoodItemService(req.body);
    res.status(201).send({ ...req.body, _id: resp.insertedId });
}

const updateFoodController = async (req, res) => {

    const permissions = getPermissions(req.me.permissionId);
    if (!permissions.editor) {
        return res.error(unauthorisedError);
    }

    const foodId = req.params.id;

    const { name } = req.body;

    const existedFood = await getFoodByNameService(name);


    if (existedFood && existedFood._id.toString() !== foodId) {
        return res.error(alreadyExistError);
    }

    const resp = await updateFoodService(foodId, req.body);

    res.send(resp);
}

const delteFoodController = async (req, res) => {
    const foodId = req.params.id;
    const resp = await deleteFoodService(foodId);

    res.send(resp);
}

module.exports = {
    getAllFoodsController,
    getFoodController,
    createFoodController,
    updateFoodController,
    delteFoodController
}